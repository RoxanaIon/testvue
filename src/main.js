import Vue from 'vue'
import App from './App.vue'
import Vue from "vue";
import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

Sentry.init({
	  Vue,
	  dsn: "https://638dc7064c9748f886707f3b94d099ba@o488153.ingest.sentry.io/5547984",
	  integrations: [
		      new Integrations.BrowserTracing(),
		    ],

	  // We recommend adjusting this value in production, or using tracesSampler
	//   // for finer control
	     tracesSampleRate: 1.0,
});
